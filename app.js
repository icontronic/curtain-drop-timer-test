'use strict';


// function unexportOnClose() { //function to run when exiting program
//   clearInterval(tm);
//   supply.writeSync(0); // Turn LED off
//   supply.unexport(); // Unexport LED GPIO to free resources
//   input.unexport(); // Unexport Button GPIO to free resources
// };

// process.on('SIGINT', unexportOnClose); //function to run when user closes using ctrl+c

var http = require('http');
var fs = require('fs');
var moment = require('moment');

// // Loading the file index.html displayed to the client
var server = http.createServer(function(req, res) {
    fs.readFile('./index.html', 'utf-8', function(error, content) {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end(content);
    });
});



// // Loading socket.io
var io = require('socket.io').listen(server);
var counter = false;
var tm = {};
var Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
var supply = new Gpio(4, 'out'); //use GPIO pin 4 as output
var in1 = new Gpio(17, 'in', 'both'); //use GPIO pin 17 as input, and 'both' button presses, and releases should be handled
var in2 = new Gpio(27, 'in', 'both'); //use GPIO pin 27 as input, and 'both' button presses, and releases should be handled
var in3 = new Gpio(22, 'in', 'both'); 
var in4 = new Gpio(10, 'in', 'both'); 
var in5 = new Gpio(9, 'in', 'both'); 
var in6 = new Gpio(11, 'in', 'both'); 
var in7 = new Gpio(5, 'in', 'both'); 
var in8 = new Gpio(6, 'in', 'both'); 

var pins = {
	1 : in1,
	2 : in2,
	3 : in3,
	4 : in4,
	5 : in5,
	6 : in6,
	7 : in7,
	8 : in8,
};

// supply.writeSync(1); //low is high
// test with minutes here
function test(min){
		//starting write high
		
		if (counter){return;}
		// supply.writeSync(0); //setting high must use low bit
		counter = true;
		var n = min*60;
		tm = setInterval(countDown,1000);
		var results = [];
		var temp = [];
		function countDown(){
				// then set to low and go to timer mode
				supply.writeSync(1); //setting low must use high bit
				n--;
				if (n <= 0){
					clearInterval(tm);
					counter = false;	
				}

				io.sockets.emit('timing', {timer : n});
				
				for (var j = 1; j < 9; j++){
					var pinRead = pins[j].readSync();
					if (n > 10 && pinRead > 0){
						// if the above 10 seconds after power down and catches the power off
						console.log('Time : ' + n);
						if (results.indexOf(j) < 0 ){
							results.push(j);
							temp.push(n);
						}
					}
					else if (n < 11) {	 //less than 10 seconds
						//depends on the second of the termination
						switch (min*60){
							case 60:
								if (n > 0 && n > 2 && pinRead > 0){
									if (results.indexOf(j) < 0 ){
										results.push(j);
										temp.push(n);
									}				
								}
							break;
							case 180:
							if (n > 0 && n > 5 && pinRead > 0){
									if (results.indexOf(j) < 0 ){
										results.push(j);
										temp.push(n);
									}				
								}
							break;
							case 300:
							if (n > 0 && n > 7 && pinRead > 0){
									if (results.indexOf(j) < 0 ){
										results.push(j);
										temp.push(n);
									}				
								}
							break;
						}
					}
					if (n == 0 && pinRead <= 0){
						if (results.indexOf(j) < 0 ){
							results.push(j);
							temp.push(n);
						}				
					}	
				}
				if (results.length > 0 && n == 0){
					var obj = {};
					var msg = [];
					for (var k in results){
						var txt = 'Pin ' + results[k] + ' open at last ' + temp[k] + ' seconds';
						msg.push(txt);
					}
					
					obj = {status : 'end', message : msg };
					io.sockets.emit('status', obj);
				}
			}
	}
//boolean to test high and low
function testWith(pin,res, end)
{
	var obj = {};
	if (!pins[pin]){
		obj = {status : 'missing', pin : pin};
	}
	else {
		var val = pins[pin].readSync();
		var tt = +(!res); //high
		
		if (val != tt)
		{
			obj = {status : 'fail', pin : pin};
		}
		else { 
			obj = {status : 'pass', pin : pin};
		}
		console.log('Value :'+val+' Expected :'+tt);
	}
	io.sockets.emit('status', obj);
}
var timeout;	

function stop(){
  clearInterval(tm);
  clearTimeout(timeout);
  counter = false;
  // supply.writeSync(1); // Turn LED off
  // supply.unexport(); // Unexport LED GPIO to free resourcesssss
  // input.unexport(); // Unexport Button GPIO to free resources
  io.sockets.emit('timing', {timer : 0});
  io.sockets.emit('status', {status : 'stop'});
}

function checkRelay(cb){
	io.sockets.emit('status', {status : 'progress', pin : 'all'});
	supply.writeSync(0); //setting high output and then set lowoutput
	timeout = setTimeout(function(){
		//monitor for 3 sedconds then set back to the normal
		cb(); //return to timeout settings
	}, 3000);
}

io.sockets.on('connection', function (socket, username) {
    console.log('connected');
    // When the client connects, send them some message

    function errorMsg(msg)
    {
    	 socket.emit('error', { msg: msg });
    }


    // on start action
    socket.on('action', function (m) {
    	//get the min status
    	if (m.min){
    		//check for the relay lighting here
    		checkRelay(function(){
    			test(m.min);
    		});
    
    	}
    	else {
    		errorMsg("Didn't set the min for the test!");
    	}
    }); 

    	socket.on('stop', function(m){
    			//clear all Interval
    			stop();
		});
});




server.listen(8088);